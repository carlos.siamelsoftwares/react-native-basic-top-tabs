import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import MainTabs, { MainTabsObjects } from "./components/MainTabs";
import { useState } from "react";
import { RenderTabsProps } from "./components/RenderTabs";

export default function App() {
  const [tabs, setTabs] = useState<MainTabsObjects[]>([
    {
      nameScreem: "Home",
      componentScreem: (
        <View style={[styles.container, { backgroundColor: "#ccc" }]}>
          <Text>Tela Home</Text>
        </View>
      ),
    },
    {
      nameScreem: "Tela 2",
      componentScreem: (
        <View style={[styles.container, { backgroundColor: "#ccc" }]}>
          <Text>Tela 2</Text>
        </View>
      ),
    },
    {
      nameScreem: "Tela 3",
      componentScreem: (
        <View style={[styles.container, { backgroundColor: "#ccc" }]}>
          <Text>Tela 3</Text>
        </View>
      ),
    },
  ]);

  const RenderCustom = ({
    onTabPress,
    tabs,
    activeTab,
    backgroundColor,
    textColor,
  }: RenderTabsProps) => {
    return (
      <View style={{ backgroundColor: backgroundColor }}>
        <View
          style={{
            flexDirection: "row",
            width: "100%",
            height: 50,
            justifyContent: "flex-start",
          }}
        >
          {tabs.map((tab, index) => {
            return (
              <TouchableOpacity
                onPress={() => onTabPress(index)}
                key={tab.nameScreem}
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "row",
                  flex: 1,
                }}
              >
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Text style={{ color: textColor }}>{tab.nameScreem}</Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={[styles.container]}>
      <MainTabs
        renderTabs={RenderCustom}
        tabProps={{
          hideIndicator: false,
          colorIndicator: "#2a7886",
          backgroundColor: "#fff",
          textColor: "#000",
        }}
        defaultIndex={2}
        tabs={tabs}
      />
      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
