import { Dimensions, Text, TouchableOpacity, View } from "react-native";
import { MainTabsObjects } from "../MainTabs";
import { useState } from "react";

export interface RenderTabsProps {
  tabs: MainTabsObjects[];
  onTabPress: (index: number) => void;
  backgroundColor?: string;
  textColor?: string;
  activeTab?: number;
}
function RenderTabs({
  tabs,
  backgroundColor = "#fff",
  onTabPress,
  textColor,
}: RenderTabsProps) {
  return (
    <View style={{ backgroundColor: backgroundColor }}>
      <View
        style={{
          flexDirection: "row",
          width: "100%",
          height: 50,
          justifyContent: "flex-start",
        }}
      >
        {tabs.map((tab, index) => {
          return (
            <TouchableOpacity
              onPress={() => onTabPress(index)}
              key={tab.nameScreem}
              style={{
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "row",
                flex: 1,
              }}
            >
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text style={{ color: textColor }}>{tab.nameScreem}</Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
}

export default RenderTabs;
