import { Dimensions, FlatList, Text, View } from "react-native";
import { MainTabsProps } from "../MainTabs";
import {
  forwardRef,
  useCallback,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import Animated, {
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
} from "react-native-reanimated";

export interface MainTabsRef {
  setActiveIndexTab: (index: number) => void;
}

const RenderScreems = forwardRef<MainTabsRef, MainTabsProps>(
  ({ tabs, onIndexChange, defaultIndex = 0, tabProps }, ref) => {
    const programmaticScroll = useRef<number | undefined>(undefined);
    const flatLisRef = useRef<FlatList>(null);
    const [activeTab, setActiveTab] = useState<number>(defaultIndex);
    const widthScreen = Dimensions.get("screen").width;
    const widthItem = widthScreen / tabs.length;
    const quantityTabs = tabs.length;
    const animateTab = useSharedValue(0);

    const tabStyle = useAnimatedStyle(() => {
      return {
        transform: [
          {
            translateX: animateTab.value,
          },
        ],
      };
    });

    const scrolAnimated = useAnimatedScrollHandler((event) => {
      animateTab.value = event.contentOffset.x / quantityTabs;
    });

    const setActiveIndexTab = useCallback(
      (index: number) => {
        programmaticScroll.current = index;
        flatLisRef.current?.scrollToIndex({ index, animated: true });
      },
      [activeTab]
    );

    useEffect(() => {
      onIndexChange && onIndexChange(activeTab);
    }, [activeTab]);

    const getItemLayout = (_: any, index: number) => ({
      length: widthScreen,
      offset: widthScreen * index,
      index,
    });

    useImperativeHandle(ref, () => ({
      setActiveIndexTab,
    }));

    return (
      <View style={{ flex: 1, flexDirection: "column" }}>
        {!tabProps.hideIndicator && (
          <View
            style={{ height: 6, backgroundColor: tabProps.backgroundColor }}
          >
            <Animated.View
              style={[
                {
                  width: widthItem,
                  flex: 1,
                  borderRadius: 3,
                  backgroundColor: tabProps.colorIndicator,
                },
                tabStyle,
              ]}
            />
          </View>
        )}

        <Animated.FlatList
          initialScrollIndex={defaultIndex}
          ref={flatLisRef}
          scrollEventThrottle={16}
          data={tabs}
          renderItem={({ item }) => (
            <View style={{ width: widthScreen }}>{item.componentScreem}</View>
          )}
          onScroll={scrolAnimated}
          getItemLayout={getItemLayout}
          pagingEnabled
          horizontal
          showsHorizontalScrollIndicator={false}
          viewabilityConfig={{
            itemVisiblePercentThreshold: 50,
          }}
          onViewableItemsChanged={({ viewableItems }) => {
            if (
              programmaticScroll.current === viewableItems[0]?.index ||
              programmaticScroll.current === undefined
            ) {
              programmaticScroll.current = undefined;
              if (viewableItems[0]?.index === undefined) {
                setActiveTab(activeTab);
              } else {
                setActiveTab(viewableItems[0]?.index || 0);
              }
            }
          }}
        />
      </View>
    );
  }
);

export default RenderScreems;
