import React, { ReactNode, useRef, useState } from "react";
import { Dimensions, Text, View } from "react-native";
import RenderTabs, { RenderTabsProps } from "../RenderTabs";
import RenderScreems, { MainTabsRef } from "../RenderScreems";

export interface MainTabsObjects {
  nameScreem: string;
  componentScreem: JSX.Element;
}

export interface TabProps {
  hideIndicator?: boolean;
  colorIndicator?: string;
  backgroundColor?: string;
  textColor?: string;
}

export interface MainTabsProps {
  tabs: MainTabsObjects[];
  onIndexChange?: (index: number) => void;
  defaultIndex?: number;
  tabProps: TabProps;
  renderTabs?: (render: RenderTabsProps) => ReactNode;
}
function MainTabs({
  tabs,
  onIndexChange,
  defaultIndex = 0,
  tabProps = {
    hideIndicator: false,
    colorIndicator: "blue",
    backgroundColor: "#fff",
    textColor: "#000",
  },
  renderTabs: RenderTabsCustom,
}: MainTabsProps) {
  const [activeIndexTab, setActiveIndexTab] = useState(defaultIndex);
  const ref = useRef<MainTabsRef>(null);

  return (
    <View
      style={{
        width: "100%",
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center",
      }}
    >
      {RenderTabsCustom ? (
        <RenderTabsCustom
          textColor={tabProps.textColor}
          tabs={tabs}
          activeTab={activeIndexTab}
          backgroundColor={tabProps.backgroundColor}
          onTabPress={(index) => {
            ref.current?.setActiveIndexTab(index);
            setActiveIndexTab(index);
          }}
        />
      ) : (
        <RenderTabs
          textColor={tabProps.textColor}
          tabs={tabs}
          activeTab={activeIndexTab}
          backgroundColor={tabProps.backgroundColor}
          onTabPress={(index) => {
            ref.current?.setActiveIndexTab(index);
            setActiveIndexTab(index);
          }}
        />
      )}

      <RenderScreems
        ref={ref}
        defaultIndex={defaultIndex}
        tabProps={tabProps}
        onIndexChange={(index) => {
          onIndexChange && onIndexChange(index);
          setActiveIndexTab(index);
        }}
        tabs={tabs}
      />
    </View>
  );
}

export default MainTabs;
